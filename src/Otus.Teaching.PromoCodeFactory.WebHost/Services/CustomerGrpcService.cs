using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Proto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    /// <summary>
    /// gRPC-сервис для работы с клиентами
    /// </summary>
    public class CustomerGrpcService : CustomerGrpc.CustomerGrpcBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerGrpcService(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenceRepository)
        {
            this._customerRepository = customerRepository;
            this._preferenceRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Добавление нового клиента
        /// </summary>
        public override async Task<CustomerGrpcResponse> CreateCustomerAsync(CreateOdEditCustomerGrpcRequest request,
                                                                             ServerCallContext context)
        {
            var preferencesIds = request.PreferenceIds.Select(x => Guid.Parse(x.Value)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            var customer = new Customer();
            customer.Id = Guid.NewGuid();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.AddAsync(customer);

            var response = new CustomerGrpcResponse()
            {
                Id = new Id { Value = customer.Id.ToString() },
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };
            
            return response;
        }

        /// <summary>
        /// Удаление существующего клиента по Id
        /// </summary>
        public override async Task<Empty> DeleteCustomerAsync(Id request,
                                                              ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Value));

            if (customer == null)
            {
                var metadata = new Metadata
                    {
                        { "customerId: ", customer.Id.ToString() }
                    };

                throw new RpcException(new Status(StatusCode.NotFound, "Permission denied"), metadata);
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        /// <summary>
        /// Изменение данных существующего клиента
        /// </summary>
        public override async Task<Empty> EditCustomersAsync(CreateOdEditCustomerGrpcRequest request,
                                                             ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id.Value));

            if (customer == null)
            {
                var metadata = new Metadata 
                {
                    { "customerId: ", customer.Id.ToString() }
                };

                throw new RpcException(new Status(StatusCode.NotFound, "Permission denied"), metadata);
            }

            var preferencesIds = request.PreferenceIds.Select(x => Guid.Parse(x.Value)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        /// <summary>
        /// Получение данных клиента по Id
        /// </summary>
        public override async Task<CustomerGrpcResponse> GetCustomerAsync(Id request,
                                                                          ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Value));

            var response = new CustomerGrpcResponse()
            {
                Id = new Id { Value = customer.Id.ToString() },
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };

            if (customer.Preferences.Count <= 0)
            {
                return response;

            }
            foreach (var preference in customer.Preferences)
            {
                response.Preferences.Add(new PreferenceGrpcResponse()
                {
                    Id = new Id { Value = preference.PreferenceId.ToString() },
                    Name = preference.Preference.Name
                });
            }

            return response;
        }
        
        /// <summary>
        /// Получение данных всех клиентов
        /// </summary>
        public override async Task GetCustomersAsync(Empty request,
                                                     IServerStreamWriter<CustomerGrpcShortResponse> responseStream,
                                                     ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerList = customers.Select(x => new CustomerGrpcShortResponse()
            {
                Id = new Id { Value = x.Id.ToString() },
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            foreach (var customer in customerList)
            {
                await responseStream.WriteAsync(customer);
            }
        }
    }
}