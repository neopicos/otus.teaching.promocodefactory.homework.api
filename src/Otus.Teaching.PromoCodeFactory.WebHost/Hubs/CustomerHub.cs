using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class CustomerHub : Hub
    {
        public async Task<string> SayHello(string name)
        {
            return $"Hello {name}";
        }
    }
}