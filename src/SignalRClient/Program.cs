﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder().WithUrl("https://localhost:5001/Hubs/CustomerHub")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            var result = await connection.InvokeAsync<string>("SayHello", "dev. neopicos");
        }
    }
}