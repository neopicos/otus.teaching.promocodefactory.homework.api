﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    /// <summary>
    /// Класс-посредник, предоставляющий текущую дату
    /// </summary>
    public class CurrentDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}