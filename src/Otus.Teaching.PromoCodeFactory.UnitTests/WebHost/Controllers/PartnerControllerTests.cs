﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers
{
    /// <summary>
    /// Класс тестов контроллера API партнеров
    /// </summary>
    public class PartnerControllerTests
    {
        private readonly Mock<IRepository<Partner>> _mockPartnerRepository;
        private readonly Mock<ICurrentDateTimeProvider> _mockCurrentDateTimeProvider;
        private readonly PartnersController _partnersController;

        public PartnerControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _mockPartnerRepository = fixture.Freeze<Mock<IRepository<Partner>>>();
            _mockCurrentDateTimeProvider = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();

            _partnersController = fixture.Build<PartnersController>()
                .OmitAutoProperties()
                .Create();
        }

        /// <summary>
        /// Метод установки партнеру лимита в случае отсутствия партера должен возвращать NotFound (404)
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ShouldReturnNotFound()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var request = new SetPartnerPromoCodeLimitRequest();
            _mockPartnerRepository
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(default(Partner));
            
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        /// <summary>
        /// Метод установки партнеру лимита в случае неактивности партнера должен вернуть BadRequestObjectResult (400) 
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldReturnNotFound()
        {
            // Arrange
            var notActivePartner = PartnerBuilder
                .CreateBasePartner()
                .SetAsNotActive();
            var request = new SetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(notActivePartner.Id, request);
            
            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldNullifyNumberIssuePromoCodes()
        {
            // Arrange
            const int numIssuedPromoCodes = 100;
            var partnerId = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner()
                .WithOnlyOneActiveLimit()
                .WithNumberIssuedPromoCodes(numIssuedPromoCodes);

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 10)
            };

            _mockPartnerRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 10));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasNotActiveLimit_ShouldNotNullifyNumberIssuedPromoCodes()
        {
            // Arrange
            const int numIssuedPromoCodes = 100;
            var partnerId = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner()
                .WithNotActiveLimit()
                .WithNumberIssuedPromoCodes(numIssuedPromoCodes);
            
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 10)
            };

            _mockPartnerRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 10));
            
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(numIssuedPromoCodes);
        }
        
        /// <summary>
        /// Метод установки партнеру лимита сохраняет новый лимит в базу
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldAddNewLimitAndSaveInDB()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimitId = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner();
            
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 10)
            };

            _mockPartnerRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 10));


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            partner.PartnerLimits.Should().HaveCount(2);
            
            partner.PartnerLimits.Where(x => x.Id == partnerPromoCodeLimitId)
                .Should().NotBeNull();
            
            _mockPartnerRepository.Verify(x => x.UpdateAsync(partner));
        }
    }
}