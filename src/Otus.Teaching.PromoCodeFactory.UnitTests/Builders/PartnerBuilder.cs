﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    /// <summary>
    /// Класс-строитель партнера
    /// </summary>
    public static class PartnerBuilder
    {
        /// <summary>
        /// Построение сущности партнера
        /// </summary>
        /// <returns>Сущность партнера</returns>
        public static Partner CreateBasePartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 08, 10),
                        EndDate = new DateTime(2020, 10, 10),
                        Limit = 100
                    }
                }
            };
        }

        /// <summary>
        /// Построение сущности партнера с определенным колличеством промокодов
        /// </summary>
        /// <param name="partner">Сущность партнера</param>
        /// <param name="numberPromoCodes">Количество промокодов</param>
        /// <returns>Сущность партнера</returns>
        public static Partner WithNumberIssuedPromoCodes(this Partner partner, int numberPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberPromoCodes;

            return partner;
        }
        
        /// <summary>
        /// Построение сущности партнера с неактивным статусом
        /// </summary>
        /// <param name="partner">Сущность партнера</param>
        /// <returns>Сущность партнера</returns>
        public static Partner SetAsNotActive(this Partner partner)
        {
            partner.IsActive = false;

            return partner;
        }

        /// <summary>
        /// Построение сущности партнера с неактивным лимитом
        /// </summary>
        /// <param name="partner">Сущность партнера</param>
        /// <returns>Сущность партнера</returns>
        public static Partner WithNotActiveLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("c9bef066-3c5a-4e5d-9cff-bd54479f075e"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    CancelDate = new DateTime(2020, 8, 9),
                    Limit = 100
                }
            };

            return partner;
        }
        
        /// <summary>
        /// Построение сущности партнера только с одним активным лимитом
        /// </summary>
        /// <param name="partner">Сущность партнера</param>
        /// <returns>Сущность партнера</returns>
        public static Partner WithOnlyOneActiveLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            };

            return partner;
        }
    }
}